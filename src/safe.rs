//! impls that don't need `unsafe` and aren't relied on by unsafe
//!
//! Also, note that the methods here are ordered *after* the ones in `lib.rs`.

#![forbid(unsafe_code)]

use super::*;

// import this here, to avoid confusion with atomics in the unsafe parts
use core::cmp::Ordering;

impl<T, const N: usize> Default for FixedCapacityVec<T, N> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T, const N: usize> Debug for FixedCapacityVec<T, N>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.deref().fmt(f)
    }
}

impl<T, const N: usize> Clone for FixedCapacityVec<T, N>
where
    T: Clone,
{
    fn clone(&self) -> Self {
        let mut out = Self::new();
        for item in &**self {
            out.push(item.clone())
        }
        out
    }
}

impl<T, const N: usize> Hash for FixedCapacityVec<T, N>
where
    T: Hash,
{
    fn hash<H>(&self, h: &mut H)
    where
        H: std::hash::Hasher,
    {
        (**self).hash(h)
    }
}

macro_rules! impl_comparison_trait { { $Trait:ident $( $fn:ident $result:ty )? } => {
    impl<T, const N: usize> $Trait for FixedCapacityVec<T, N>
    where
        T: $Trait,
    {
        $(
            fn $fn(&self, other: &Self) -> $result {
                (**self).$fn(&**other)
            }
        )?
    }
} }

// Limited version, with just Self as RHS, mostly so we can be `Ord`
impl_comparison_trait! { PartialEq eq bool }
impl_comparison_trait! { PartialOrd partial_cmp Option<Ordering> }
impl_comparison_trait! { Ord cmp Ordering }
impl_comparison_trait! { Eq }

/// Error returned by [`try_push_or_discard()`](FixedCapacityVec::try_push_or_discard)
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct FullError;

impl Display for FullError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt("pushed to a full FixedCapacityVec", f)
    }
}
