# Release Documentation - changelog

## Changelog

### 1.0.1

 * Two minor docs fixes.

### 1.0.0

 * Minor docs improvements.
 * Declare it stable.

### 0.1.0

 * Check for allocator failures (soundness fix).
 * Significantly rework the implementation.
 * Significantly extend the APIs.
 * CI tests including miri tests.

### 0.0.1

 * Preview release only.
 * Initial import of code from an inner module in arti.git.
